﻿//Surface area of pizzas
var pizza1area = Math.PI * Math.pow((13/2),2);
var pizza2area = Math.PI * Math.pow((17/2),2);

// 2. What is the cost per square inch of each pizza?

var pizza1costpersquareinch = 16.99/pizza1area;
var pizza2costpersquareinch = 19.99/pizza2area;

//Get random card
var cards = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
function drawCard(cards) {
  var randomindex = Math.floor(Math.random() * 13) + 1;
  return cards[randomindex - 1];
}

var rando = drawCard(cards);

//Get array of three random cards and get max

var drawncards = [];
for (i = 0; i < 3; i++) {
  drawncards[i] = drawCard(cards);
}

var arrayofthreecards = drawncards.toString();

var highestcard = Math.max.apply(Math, drawncards);

var htlmmath = `
<h3>Pizza and Cards</h3>
<div>
pizza 1 area: ${pizza1area.toFixed(2)}<br/>
pizza 2 area: ${pizza2area.toFixed(2)}<br/>
pizza 1 cost per square inch: ${pizza1costpersquareinch.toFixed(2)}<br/>
pizza 2 cost per square inch: ${pizza2costpersquareinch.toFixed(2)}<br/><br/>
random card: ${rando}<br/>
array of 3 cards: ${arrayofthreecards}<br/>
max card: ${highestcard}
</div><br/><br/>`;
document.getElementById('math').innerHTML = htlmmath;

//Address

var firstName = "Christina", lastName = "Nock", streetAddress = "some street address", city = "some city", state = "wa", zipCode = "somezipcode";
var mystring = `${firstName} ${lastName}
${streetAddress}
${city}, ${state} ${zipCode}`;
//Extract first name
var longstring = `firstName lastName
streetAddress
city, state zip`;
var extrstring = longstring.substr(0,longstring.indexOf(' '));

var htlmaddress = `
<h3>Address</h3>
<div>
Address block: ${mystring}<br/>
Just firstname: ${extrstring}<br/>
</div><br/><br/>`;
document.getElementById('address').innerHTML = htlmaddress;

//Mid date
const startDate = new Date(2020, 1, 1);
const endDate = new Date(2020, 4, 1);
var middate = new Date((startDate.getTime() + endDate.getTime()) / 2);
var htlmdate = `
<h3>Date</h3>
<div>
Mid date: ${middate}<br/>
</div><br/><br/>`;
document.getElementById('date').innerHTML = htlmdate;